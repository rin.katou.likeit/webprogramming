<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー削除確認</title>
</head>
<body>

    <center>
   <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
  			</li>
  				</center>
    </center>

     <div class="col-sm-4">
            <div style="text-align: right">
     <p><a href="loginServlet" target="_blank">ログアウト</a></p>
         </div>
    </div>

      <div class="container">
    <h1 style="text-align:center">ユーザー削除確認</h1>
          </div>

     <div class="row ">
        <div class="col-sm-3">
            <div style="text-align: reft">
    <p>ログインID: ${useridInfo.loginId}</p>

             <div style="text-align: reft">
    <p>を本当に削除してよろしいでしょうか。</p>
            </div>
         </div>
    </div>
    </div>

<form class="form-signin" action="UserListServlet" method="get">

    <div class="container">
  <div class="row">
    <div class="col">
       <input type="submit" value="キャンセル" class="btn btn-primary">
    </div>
        </div>
        </form>

  <form class="form-signin" action="UserDeleteServlet" method="post">
  <input type="hidden" name="login_id" value="${useridInfo.loginId}">
    <div class="col">
      <input type="submit" value="はい" class="btn btn-primary">
    </div>
  </div>
  </form>

</body>
</html>