<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報詳細参照</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/csss.css" rel="stylesheet" type="text/css" />
</head>
<body>

    <center>
     <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
  			</li>
  				</center>
    </center>

     <div class="col-sm-10">
            <div style="text-align: right">
     <p><a href="loginServlet" target="_blank">ログアウト</a></p>
         </div>
    </div>

   <div class="container">
    <h1 style="text-align:center">ユーザー情報詳細参照</h1>
    </div>

      <div class="row login">
        <div class="col-sm-6">
            <div style="text-align: right">
            <h2>ログインID</h2>
            </div>
        </div>
        <div class="col-sm-6">
            <p>${useridInfo.loginId}</p>
        </div>
    </div>
    <div class="row password">
          <div class="col-sm-6">
              <div style="text-align: right">
        <h2>ユーザー名</h2>
        </div>
    </div>
        <div class="col-sm-6">
            <p>${useridInfo.name}</p>
    </div>
    </div>
       <div class="row password">
          <div class="col-sm-6">
              <div style="text-align: right">
        <h2>生年月日</h2>
        </div>
    </div>
        <div class="col-sm-6">
            <p>${useridInfo.birthDate}</p>
    </div>
    </div>

     <div class="row password">
          <div class="col-sm-6">
              <div style="text-align: right">
        <h2>登録日時</h2>
        </div>
    </div>
        <div class="col-sm-6">
            <p>${useridInfo.createDate}</p>
    </div>
    </div>
     <div class="row date">
        <div class="col-sm-6">
            <div style="text-align: right">
            <h2>更新日時</h2>
            </div>
        </div>

          <div class="col-sm-6">
              <p>${useridInfo.updateDate}</p>
        </div>
    </div>

    <p><a href="UserListServlet" target="_blank">戻る</a></p>

    </body>
</html>