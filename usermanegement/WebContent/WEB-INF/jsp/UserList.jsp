<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/csss.css" rel="stylesheet" type="text/css" />
</head>
<body>

	<center>
          <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
  			</li>
  				</center>

	<div class="col-sm-10">
		<div style="text-align: right">
			<p>
			  <a href="loginServlet" class="navbar-link logout-link">ログアウト</a>

			</p>
		</div>
	</div>

	<div class="container">
		<h1 style="text-align: center">ユーザー覧</h1>
	</div>

	<div class="col-sm-10">
		<div style="text-align: right">
			<p>
				<a href="NewUserServlet" target="_blank">新規登録</a>
			</p>

<form class="form-signin" action="UserListServlet" method="post">
			<div class="row login">
				<div class="col-sm-6">
					<div style="text-align: right">
						<h2>ログインID</h2>
					</div>
				</div>
				<div class="col-sm-6">
					<input type="text" name = "login_id">
				</div>
			</div>
			<div class="row password">
				<div class="col-sm-6">
					<div style="text-align: right">
						<h2>ユーザーネーム</h2>
					</div>
				</div>
				<div class="col-sm-6">
					<input type="text"  name = "name">
				</div>
			</div>


			<div class="container seinengappi">
				<div class="row justify-content-md-center">
					<div class="col col-lg-2">
						<h2>生年月日</h2>
					</div>
					<div class="col-md-auto">
						<input type="date" name = "birth_date">~
						<div class="col-md-auto"></div>

					</div>
					<div class="col col-lg-2">
						<input type="date"  name = "birth_dateC">
					</div>
				</div>


				<div class="row kensaku">
					<div class="col-sm-10">
						<div style="text-align: right">
							<input type="submit" value="検索" class="btn btn-primary">
						</div>
					</div>
				</div>
</form>

				<div class="row login"></div>
				<center>
					<table border="1 ">
						<tr>
							<th>ログインID</th>
							<th>ユーザー名</th>
							<th>生年月日</th>
							<th></th>
						</tr>
						<tbody>
							<c:forEach var="user" items="${userList}">
								<tr>
									<td>${user.loginId}</td>
									<td>${user.name}</td>
									<td>${user.birthDate}</td>




									<td><a class="btn btn-primary"href="UserRefoServlet?id=${user.id}" target="_blank">詳細</a>
									<c:if test="${userInfo.loginId==user.loginId or userInfo.loginId=='admin'}">
									    <a class="btn btn-success" href="UserinfoUpdServlet?id=${user.id}" target="_blank">更新</a>
									     </c:if>
									    	<c:if test="${userInfo.loginId=='admin'}">
										<a class="btn btn-danger"href="UserDeleteServlet?id=${user.id}" target="_blank">削除</a></td>
                                    </c:if>
								</tr>
							</c:forEach>
						</tbody>
					</table>
			</div>
		</div>
	</div>
	</div>
	</center>
</body>
</html>