<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー新規登録</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/csss.css" rel="stylesheet" type="text/css" />
</head>
<body>


    <center>
  <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
  			</li>
  				</center>
 	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
    </center>


     <div class="col-sm-10">
            <div style="text-align: right">
     <p><a href="loginServlet" target="_blank">ログアウト</a></p>
         </div>
    </div>



   <div class="container">
    <h1 style="text-align:center">ユーザー新規登録</h1>
    </div>
<form class="form-signin" action="NewUserServlet" method="post">

      <div class="row login">
        <div class="col-sm-6">
            <div style="text-align: right">
            <h2>ログインID</h2>
            </div>
        </div>
        <div class="col-sm-6">
            <input type="id" name = "loginId">
        </div>
    </div>
    <div class="row password">
          <div class="col-sm-6">
              <div style="text-align: right">
        <h2>パスワード</h2>
        </div>
    </div>
        <div class="col-sm-6">
        <input type="password" name = "password">
    </div>
    </div>
       <div class="row password">
          <div class="col-sm-6">
              <div style="text-align: right">
        <h2>パスワード(確認)</h2>
        </div>
    </div>
        <div class="col-sm-6">
        <input type="password" name = "passwordC">
    </div>
    </div>

     <div class="row password">
          <div class="col-sm-6">
              <div style="text-align: right">
        <h2>ユーザー名</h2>
        </div>
    </div>
        <div class="col-sm-6">
        <input type="text" name = "name">
    </div>
    </div>
     <div class="row date">
        <div class="col-sm-6">
            <div style="text-align: right">
            <h2>生年月日</h2>
            </div>
        </div>

          <div class="col-sm-6">
            <input type="date" name ="birthDate">
        </div>
    </div>
    <input type="submit" value="登録" class="btn btn-primary">
</form>

<center>
 <div class="row touroku">
       <div class = "container">

    </div>
    </div>
    </center>

   <p><a href="UserListServlet" target="_blank">戻る</a></p>


</body>
</html>