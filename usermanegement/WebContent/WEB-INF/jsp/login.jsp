<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/csss.css" rel="stylesheet" type="text/css" />
</head>
<body>
   <div class="container">
    <h1 style="text-align:center">ログイン画面</h1>
    </div>

 <center>
 	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	 </center>
<form class="form-signin" action="loginServlet" method="post">
      <div class="row login">
        <div class="col-sm-6">
            <div style="text-align: right">
            <h2>ログインID</h2>
            </div>
        </div>
        <div class="col-sm-6">
            <input type="text" name="loginId">
        </div>
    </div>
    <div class="row password">
          <div class="col-sm-6">
              <div style="text-align: right">
        <h2>パスワード</h2>
        </div>
    </div>
        <div class="col-sm-6">
        <input type="password" name="password">
    </div>
    </div>

 <div class="row kensaku">
       <div class = "container">
      <input type="submit" value="ログイン" class="btn btn-primary">
    </div>
    </div>


 </form>


</body>
</html>