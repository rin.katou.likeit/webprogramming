package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserinfoUpdServlet
 */
@WebServlet("/UserinfoUpdServlet")
public class UserinfoUpdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserinfoUpdServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");

		UserDao userDao = new UserDao();
		User userid = userDao.findByUserinfo(id);


		request.setAttribute("useridInfo", userid);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserinfoUpd.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		   request.setCharacterEncoding("UTF-8");

			// リクエストパラメータの入力項目を取得

			String name = request.getParameter("name");
			String password = request.getParameter("password");
			String passwordC = request.getParameter("passwordC");
			String id = request.getParameter("id");
			String birthdate = request.getParameter("birth_date");

		UserDao userDao = new UserDao();
		User userid = userDao.findByUserinfo(id);

		if (password.equals("")&&(passwordC.equals(""))) {
			userDao.UserUpdate2(name,birthdate,id);

		}

		if (!(password.equals(passwordC))) {
			request.setAttribute("useridInfo", userid);
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserinfoUpd.jsp");
			dispatcher.forward(request, response);
			return;

		}
		if (name.equals("")||(birthdate.equals(""))) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("useridInfo", userid);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserinfoUpd.jsp");
			dispatcher.forward(request, response);
			return;

		}
		userDao.UserUpdate(name,password,birthdate,id);
		response.sendRedirect("UserListServlet");
	}

}

