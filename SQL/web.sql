CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

USE usermanagement;

CREATE TABLE user(
id SERIAL,
login_id varchar(256) NOT NULL,
name varchar(256) NOT NULL,
birth_date DATE NOT NULL,
password varchar(256) NOT NULL,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL);

INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUE('admin','管理者','2020-01-01','password',NOW(),NOW());
